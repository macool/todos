# encoding: utf-8
class TodosController < ApplicationController
  
  before_filter :require_login

  def index
    @new_todo ||= Todo.new
    @todos = current_user.todos.order("id DESC")
  end
  
  def create
    @new_todo = Todo.new params[:todo]
    @new_todo.user = current_user
    if @new_todo.save
      respond_to do |format|
        format.html {
          flash["alert-success"] = "Guardado."
          redirect_to action: :index
        }
        format.js
      end
    else
      respond_to do |format|
        format.html {
          flash["alert-error"] = "Hubo un problema."
          render :index
        }
        format.js
      end
    end
  end
  
  def cambiar_estado
    if @todo = current_user.todos.find_by_id(params[:id])
      if @todo.completado
        @todo.completado = false
      else
        @todo.completado = true
      end
      @todo.save
      render :text => "#{@todo.completado}"
    else
      render :text => "estás haciendo travesuras?"
    end
  end
  
  def destroy
    if @todo = current_user.todos.find_by_id(params[:id])
      @todo.destroy
      respond_to do |format|
        format.js {
          render :text => "Ok"
        }
      end
    else
      render :text => "¿Sigues haciendo travesuras?"
    end
  end
  
end
