# encoding: utf-8
class UserSessionsController < ApplicationController
  
  def new
    if current_user
      redirect_to :controller => :todos, :action => :index
    end
  end
  
  def create
    if @user = login(params[:username], params[:password])
      session[:user_id] = @user.id
      flash["alert-success"] = "Has iniciado sesión"
      redirect_to root_path
    else
      flash["alert-error"] = "Nombre de usuario o contraseña inválidos"
      render :new
    end
  end
  
  def destroy
    session[:user_id] = nil
    flash["alert-notice"] = "Cerraste sesión"
    redirect_to root_path
  end
  
end
