unless window.Helpers
  window.Helpers = {}
  
window.Helpers.Notification = {
  create: (kind, content) ->
    $("#alerts}").prepend("<div class='#{kind}'>#{content}<span class='close_alert'>X</span></div>")
}

$(".close_alert").live "click", ->
  $(this).parent().slideUp("slow")