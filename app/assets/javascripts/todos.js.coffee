TodosController = {
  cambiar_estado_todo: ->
    $checkbox = $(this)
    $todo_text = $checkbox.prev()
    id = $checkbox.parent().data("todo_id")
    $.get "/todos/#{id}/cambiar_estado", (data) ->
      if data == "true"
        $checkbox.attr("checked","checked")
        $todo_text.css("text-decoration","line-through")
      else
        $checkbox.removeAttr "checked"
        $todo_text.css("text-decoration","none")
        
  eliminar_todo: ->
    $this = $(this)
    id = $this.parent().data "todo_id"
    $.post "/todos/#{id}", {_method: 'delete'}
    $this.parent().animate {
      marginLeft: 200,
      opacity: 0
    }, "fast", ->
      $this.parent().remove()
    
  init: ->
    $(".todo_checkbox").live "click", this.cambiar_estado_todo
    $(".eliminar_todo").live "click", this.eliminar_todo
}

$(document).on "ready", ->
  TodosController.init()