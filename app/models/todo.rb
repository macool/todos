class Todo < ActiveRecord::Base
  attr_accessible :descripcion
  
  validates :descripcion, :presence => true
  validates :user_id, :presence => true

  belongs_to :user
  
end
