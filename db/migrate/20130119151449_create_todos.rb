class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      
      t.string :descripcion, :null => false
      t.boolean :completado, :default => false
      
      t.timestamps
    end
  end
end
