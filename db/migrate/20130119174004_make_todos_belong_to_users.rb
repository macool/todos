class MakeTodosBelongToUsers < ActiveRecord::Migration
  def up
    change_table :todos do |t|
      t.references :user, :null => false
    end
  end

  def down
    change_table :todos do |t|
      t.remove :user_id
    end
  end
end
