class AddFieldsToUSers < ActiveRecord::Migration
  def up
    change_table :users do |t|
      t.string :name
    end
  end
  def down
    change_table :users do |t|
      t.remove :name
    end
  end
end
